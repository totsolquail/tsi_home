$( document ).ready(function() {
    var spSiteUrl = 'https://totalsol.sharepoint.com/sites/allstaff';
    var listName = 'QuickLinks';
    var itemContainerClassName = 'quick-links-container';

    appendItems();

    function appendItems() {
      var itemContainer = document.querySelector("." + itemContainerClassName);
      var api_url = spSiteUrl + "/_api/web/lists/GetByTitle('" + listName + "')/items?$select=LinkTitle0,LinkUrl,AttachmentFiles&$expand=AttachmentFiles";
      $.ajax({
        url: api_url,
        contentType: "application/json",
        dataType: 'json',
        success: function(result){
          for (let key in result.value) {
            console.log("result: ")  
            console.log(result);
            var itemAnchor = document.createElement('a');
            itemAnchor.classList.add('quick-links-item-anchor');
            itemAnchor.href = result.value[key].LinkUrl;
            
            var item = document.createElement('div');
            item.classList.add('quick-links-item');

            var itemImg = document.createElement('img');
            itemImg.classList.add('quick-links-img');
            itemImg.src = "https://totalsol.sharepoint.com" + result.value[key].AttachmentFiles[0].ServerRelativeUrl;
            
            var itemTitle = document.createElement('div');
            itemTitle.classList.add('quick-links-item-title');
            itemTitle.innerHTML = result.value[key].LinkTitle0;
            
            item.appendChild(itemImg);
            item.appendChild(itemTitle);
            itemAnchor.appendChild(item);

            itemContainer.appendChild(itemAnchor);
          }  
        }
      });
    }
});