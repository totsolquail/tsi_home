$( document ).ready(function() {
  var spSiteUrl = 'https://totalsol.sharepoint.com/sites/allstaff';
  var listName = 'UrgentAnnouncements';
  var api_url = spSiteUrl + "/_api/web/lists/GetByTitle('" + listName + "')/items";
  var announcementsContainer = document.querySelector(".urgent-announcements-container");
  $.ajax({
    url: api_url,
    contentType: "application/json",
    dataType: 'json',
    success: function(result){
      for (let key in result.value) {
        var item = "<div class='.urgent-announcements-item' style='display: flex; margin-bottom: 5px'>"
        var itemCategoryLink = "<a onclick=\"window.open ('" + result.value[key].URL + "', ''); return false\" href='javascript:void(0);' style='text-decoration:none'><div class='urgent-announcements-item-category'>" + result.value[key].Category + "</div></a>";
        var itemArrowLink = "<a onclick=\"window.open ('" + result.value[key].URL + "', ''); return false\" href='javascript:void(0);'><div class='urgent-announcements-item-arrow-right'></div></a>";
        var itemBodyLink = "<a onclick=\"window.open ('" + result.value[key].URL + "', ''); return false\" href='javascript:void(0);' style='text-decoration:none'><div class='urgent-announcements-item-title'>" + result.value[key].Body + "</div></a>";
        item += itemCategoryLink;
        item += itemArrowLink;
        item += itemBodyLink;
        announcementsContainer.innerHTML += item;
      }  
    }
})
});