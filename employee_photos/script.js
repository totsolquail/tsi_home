$( document ).ready(function() {
  var spSiteUrl = 'https://totalsol.sharepoint.com/sites/allstaff';
  var listName = 'EmployeePhotos';

  appendItems('employee-photos-delivery-photos','Delivery');
  appendItems('employee-photos-sales-photos', 'Sales');
  appendItems('employee-photos-design-photos', 'Design');
  appendItems('employee-photos-operations-photos', 'Operations');
  appendItems('employee-photos-products-photos', 'Products');
  appendItems('employee-photos-hr-photos', 'HR');

  function appendItems(itemContainerClassname, departmentName) {
    var itemContainer = document.querySelector("." + itemContainerClassname);
    var api_url = spSiteUrl + "/_api/web/lists/GetByTitle('" + listName + "')/items?$filter=Department eq '" + departmentName + "'&$select=Employee/Title,Employee/JobTitle,AttachmentFiles&$expand=AttachmentFiles,Employee&$orderby=OrderBy";
    $.ajax({
      url: api_url,
      contentType: "application/json",
      dataType: 'json',
      success: function(result){
        for (let key in result.value) {
            var item = document.createElement('div');
            item.classList.add('employee-photos-item');
            
            var itemLink = document.createElement('a');
            itemLink.classList.add('employee-photos-tile');
            
            var itemImg = document.createElement('img');
            itemImg.classList.add('employee-photos-img');
            itemImg.src = "https://totalsol.sharepoint.com" + result.value[key].AttachmentFiles[0].ServerRelativeUrl;
            
            var itemTileOverlay = document.createElement('div');
            itemTileOverlay.classList.add('employee-photos-tile-overlay');
            
            var itemTileText = document.createElement('div');
            itemTileText.classList.add('employee-photos-tile-text');
            itemTileText.innerHTML = result.value[key].Employee.Title + "<br>" + result.value[key].Employee.JobTitle
            
            itemTileOverlay.appendChild(itemTileText);
            itemLink.appendChild(itemImg);
            itemLink.appendChild(itemTileOverlay);
            item.appendChild(itemLink);
            
            itemContainer.appendChild(item);
        }  
      }
    });
  }
});